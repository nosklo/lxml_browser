__title__ = 'LXML Browser'
__package_name__ = 'lxml_browser'
__author__ = 'Clovis Fabricio Costa'
__description__ = 'A headless browser written with `requests` and `lxml`'
__email__ = 'python.nosklo@0sg.net'
__version__ = '0.0.1'
__license__ = 'GPLv3'
__copyright__ = 'Copyright 2018 Clovis Fabricio Costa'
