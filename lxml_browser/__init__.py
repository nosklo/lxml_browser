#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""

LXML Browser
------------

A headless browser written with `requests` and `lxml`

"""

from __future__ import absolute_import, division, print_function, \
    with_statement, unicode_literals

from .lxml_browser import Browser
