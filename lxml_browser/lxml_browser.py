#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import (
    absolute_import, division, print_function, with_statement,
    unicode_literals
)
import collections
import urlparse
import requests
import lxml.html
from cStringIO import StringIO


def _h(element):    
    return lxml.html.tostring(element, pretty_print=True)

def _save(text):
    import tempfile
    with tempfile.NamedTemporaryFile(suffix='.html', delete=False) as f:
        f.write(text)
    return f.name

def _save_open(text):
    import subprocess
    return subprocess.call(['xdg-open', _save(text)])

class Browser(object):
    def __init__(self, proxies=None):
        self.session = requests.Session()
        if proxies:
            self.session.proxies = proxies
        self.history = collections.deque(maxlen=100)
        self.response = None

    def _fix_url(self, url):
        if self.current_url:
            url = urlparse.urljoin(self.current_url, url)
        return url

    @property
    def response(self):
        return self._response
        
    @response.setter
    def response(self, response):
        self._response = response
        self._tree = None
        self._json = None
        self.current_url = getattr(response, 'url', None)
        if self.current_url:
            self.history.append(self.current_url)

    def _requests_lxml_form_post(self, kwds):
        values_param = kwds.pop('values_param', 'data')
        def _requests_lxml_form_poster(method, url, values):
            assert method == 'POST'
            url = self._fix_url(url)
            kwds[values_param] = values
            self.response = self.session.post(url, **kwds)
            return self.response
        return _requests_lxml_form_poster

    def open(self, url, **params):
        url = self._fix_url(url)
        self.response = self.session.get(url, **params)
        return self

    def post(self, url, **params):
        url = self._fix_url(url)
        self.response = self.session.post(url, **params)
        return self

    def xpath(self, *args, **kwds):
        return self.tree.xpath(*args, **kwds)

    @property
    def text(self):
        if self.response is not None:
            return self.response.text

    @property
    def dom(self):
        return self.tree.getroot()

    @property
    def tree(self):
        if self._tree is None and self.response is not None:
            self._tree = self._parse_html()
            self._update_content_type()
        return self._tree

    def _parse_html(self, encoding=None):
        parser = lxml.html.HTMLParser(encoding=encoding)
        return lxml.html.parse(StringIO(self.response.content), parser, 
            base_url=self.current_url)

    def _update_content_type(self):
        content_type = self.xpath("//meta[@http-equiv='Content-Type']/@content")
        if content_type:
            self.content_type = [t.strip() for t in content_type[0].split(';')]
            for c in self.content_type:
                if c.startswith('charset='):
                    self._tree = self._parse_html(c[8:].strip())
        else:
            self.content_type = None

    @property
    def json(self):
        if self._json is None and self.response is not None:
            self._json = self.response.json()
        return self._json

    def submit(self, form, **kwds):
        if kwds.pop('multipart', False):
            kwds['values_param'] = 'files'
        extra_values = kwds.pop('extra_values', None)
        lxml.html.submit_form(form,
            extra_values=extra_values,
            open_http=self._requests_lxml_form_post(kwds),
        )
        return self


#class Lxml_browser(object):
#    """This is a description of the class."""

#    #: An example class variable.
#    aClassVariable = True

#    def __init__(self, argumentName, anOptionalArg=None):
#        """Initialization method.

#        :param argumentName: an example argument.
#        :type argumentName: string
#        :param anOptionalArg: an optional argument.
#        :type anOptionalArg: string
#        :returns: New instance of :class:`Lxml_browser`
#        :rtype: Lxml_browser

#        """

#        self.instanceVariable1 = argumentName

#        if self.aClassVariable:
#            print('Hello')

#        if anOptionalArg:
#            print('anOptionalArg: %s' % anOptionalArg)
