============
Installation
============

At the command line::

    $ easy_install lxml_browser

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv lxml_browser
    $ pip install lxml_browser
