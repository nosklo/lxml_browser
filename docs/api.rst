.. _api:

===
API
===

.. automodule:: lxml_browser
    :members:

    .. autoclass:: Lxml_browser
        :members:
