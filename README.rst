============
LXML Browser
============


.. image:: https://travis-ci.org/nosklo/lxml_browser.png?branch=master
    :target: https://travis-ci.org/nosklo/lxml_browser

.. image:: https://badge.fury.io/py/lxml_browser.png
    :target: http://badge.fury.io/py/lxml_browser

.. image:: https://coveralls.io/repos/nosklo/lxml_browser/badge.png?branch=master
    :target: https://coveralls.io/r/nosklo/lxml_browser?branch=master

.. image:: https://pypip.in/d/lxml_browser/badge.png
        :target: https://crate.io/packages/lxml_browser?version=latest

``lxml_browser`` - A headless browser written with `requests` and `lxml`

Features
--------

* TODO

==============  ==========================================================
Python support  Python 2.7, >= 3.3
Source          https://github.com/nosklo/lxml_browser
Docs            http://lxml_browser.rtfd.org
Changelog       http://lxml_browser.readthedocs.org/en/latest/history.html
API             http://lxml_browser.readthedocs.org/en/latest/api.html
Issues          https://github.com/nosklo/lxml_browser/issues
Travis          http://travis-ci.org/nosklo/lxml_browser
Test coverage   https://coveralls.io/r/nosklo/lxml_browser
pypi            https://pypi.python.org/pypi/lxml_browser
Ohloh           https://www.ohloh.net/p/lxml_browser
License         `BSD`_.
git repo        .. code-block:: bash

                    $ git clone https://github.com/nosklo/lxml_browser.git
install dev     .. code-block:: bash

                    $ git clone https://github.com/nosklo/lxml_browser.git lxml_browser
                    $ cd ./lxml_browser
                    $ virtualenv .env
                    $ source .env/bin/activate
                    $ pip install -e .
tests           .. code-block:: bash

                    $ python setup.py test
==============  ==========================================================

.. _BSD: http://opensource.org/licenses/BSD-3-Clause
.. _Documentation: http://lxml_browser.readthedocs.org/en/latest/
.. _API: http://lxml_browser.readthedocs.org/en/latest/api.html
